﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment17
{
    public class CoachFavoriteSport
    {
        public int CoachId { get; set; }

        public Coach Coach { get; set; }

        public int FavoriteSportsId { get; set; }

        public FavoriteSport favoriteSport { get; set; }
    }
}
