﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace assignment17.Migrations
{
    public partial class yes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SportFavorite",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    sport = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SportFavorite", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TrainerCoaches",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Sport = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainerCoaches", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "athletsTab",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    age = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    CoachId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_athletsTab", x => x.Id);
                    table.ForeignKey(
                        name: "FK_athletsTab_TrainerCoaches_CoachId",
                        column: x => x.CoachId,
                        principalTable: "TrainerCoaches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "coachFavoriteSportsTab",
                columns: table => new
                {
                    CoachId = table.Column<int>(nullable: false),
                    FavoriteSportsId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_coachFavoriteSportsTab", x => new { x.CoachId, x.FavoriteSportsId });
                    table.ForeignKey(
                        name: "FK_coachFavoriteSportsTab_TrainerCoaches_CoachId",
                        column: x => x.CoachId,
                        principalTable: "TrainerCoaches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_coachFavoriteSportsTab_SportFavorite_FavoriteSportsId",
                        column: x => x.FavoriteSportsId,
                        principalTable: "SportFavorite",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_athletsTab_CoachId",
                table: "athletsTab",
                column: "CoachId");

            migrationBuilder.CreateIndex(
                name: "IX_coachFavoriteSportsTab_FavoriteSportsId",
                table: "coachFavoriteSportsTab",
                column: "FavoriteSportsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "athletsTab");

            migrationBuilder.DropTable(
                name: "coachFavoriteSportsTab");

            migrationBuilder.DropTable(
                name: "TrainerCoaches");

            migrationBuilder.DropTable(
                name: "SportFavorite");
        }
    }
}
