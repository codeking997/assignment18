﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using assignment17.DTOs;
using assignment17.Migrations;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace assignment17.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //this class makes it possible to add, retrive, update and delete a sport from the sports table
    public class SportFavoriteController : ControllerBase
    {
        private readonly CoachDBContext _context;

        private readonly IMapper _mapper;
        //establishes a connection to the database
        public SportFavoriteController(CoachDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        //retrives a sport
        [HttpGet]
        public ActionResult<IEnumerable<FavSportDto>> GetFavSportDto()
        {
            //return _context.SportFavorite;
            FavoriteSport favoriteSport = new FavoriteSport();

            var list = new List<FavSportDto>();
            var oldList = _context.SportFavorite.ToList();
            
            foreach(FavoriteSport i in oldList)
            {
                list.Add(_mapper.Map<FavSportDto>(i));
            }
            FavSportDto favSportDto = _mapper.Map<FavSportDto>(favoriteSport);

            return list;
        }
        //adds a favorite sport
        [HttpPost]
        public ActionResult<FavoriteSport> ConsumeFavSport(FavoriteSport favoriteSport)
        {
            _context.SportFavorite.Add(favoriteSport);
            _context.SaveChanges();

            return favoriteSport;
        }
        //this method allows the user to find a sport based on the id of the sport
        [HttpGet("{id}")]

        public ActionResult<FavoriteSport> GetFavSport(int id)
        {
            var favSport = _context.SportFavorite.Find(id);
            return favSport;
        }
        //updates sports object in the table
        //finds the sport by the id, if it does not exist it returns a badrequest, otherwise it saves the changes to the database
        [HttpPut("{id}")]
        public ActionResult UpdateSport(int id, FavoriteSport favoriteSport)
        {
            if(id != favoriteSport.Id)
            {
                return BadRequest();
            }
            _context.Entry(favoriteSport).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }
        //deletes a sport object from the table, finds it by id, checks if it exists, 
        //if not it returns notfound, otherwise removes it from the DB and saves the changes.
        [HttpDelete("{id}")]
        public ActionResult<FavoriteSport> DeleteSport(int id)
        {
            var sport = _context.SportFavorite.Find(id);

            if(sport == null)
            {
                return NotFound();
            }
            _context.SportFavorite.Remove(sport);
            _context.SaveChanges();

            return sport;
        }
    }
}
