﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using assignment17.DTOs;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace assignment17.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    /*
    * this class makes it possible to retrive, create, update and delete a coach
    * in the database
    */
    public class CoachController : ControllerBase
    {

        private readonly CoachDBContext _context;

        private readonly IMapper _mapper;

        //establises a connection to the database 
        public CoachController(CoachDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        //this method creates a new coach in code
        [HttpGet]
        public ActionResult<IEnumerable<CoachDto>> GetACoachDto()
        {
            //return new Coach { FirstName = "Pedro", LastName = "Gonzales", Id = 1, Sport = "Football" };

            Coach coach = new Coach();

            var list = new List<CoachDto>();
            var dtoList = _context.TrainerCoaches.ToList();

            foreach(Coach i in dtoList)
            {
                list.Add(_mapper.Map<CoachDto>(i));
            }
            CoachDto coachDto = _mapper.Map<CoachDto>(coach);

            return list;


        }
        //this method makes it possible to add an athlet to the database
        [HttpPost]
        public ActionResult<Coach> ConsumeCoach(Coach coach)
        {
            _context.TrainerCoaches.Add(coach);
            _context.SaveChanges();
            return coach;
        }
        //this method updates info on a coach, it finds the coach by his Id and then updates the info on him
        [HttpPut("{id}")]
        public ActionResult UpDateCoach(int id, Coach coach)
        {
            if (id != coach.Id)
            {
                return BadRequest();
            }
            _context.Entry(coach).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }
        /*
         * this method allows for the deletion of a coach, finds the coach by id and then deletes it
         *the if statement checks if the id exists, if not then it returns not found
         *otherwise it removes the coach and saves the changes in the database
         */
        [HttpDelete("{id}")]
        public ActionResult<Coach> DeleteCoach(int id)
        {
            var coach = _context.TrainerCoaches.Find(id);

            if (coach == null)
            {
                return NotFound();
            }
            _context.TrainerCoaches.Remove(coach);
            _context.SaveChanges();

            return coach;
        }
    }
}
