﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace assignment17.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //this class makes possible to create, update, retrive and delete a coachfavoritesports object
    public class CoachFavSportController : ControllerBase
    {
        //establishes a connection to the database
        private readonly CoachDBContext _context;
        public CoachFavSportController(CoachDBContext context)
        {
            _context = context;
        }
        //allows for retrival of the list
        [HttpGet]
        public ActionResult<IEnumerable<CoachFavoriteSport>> GetFavCoachSport()
        {
            return _context.coachFavoriteSportsTab;
        }
        //adds a new object to the list
        [HttpPost]
        public ActionResult<CoachFavoriteSport> ConsumeFavSport(CoachFavoriteSport favoriteCoachSport)
        {
            _context.coachFavoriteSportsTab.Add(favoriteCoachSport);
            _context.SaveChanges();

            return favoriteCoachSport;
        }
        //allows for a retrival of a single object, finds it by id
        [HttpGet("{id}")]

        public ActionResult<CoachFavoriteSport> GetFavCoachSport(int id)
        {
            var favSport = _context.coachFavoriteSportsTab.Find(id);
            return favSport;
        }
        //updates the object, finds it by id if the id does not exist it returns a bad request
        //otherwise it updates the given object and saves the changes
        [HttpPut("{id}")]
        public ActionResult UpdateSport(int id, CoachFavoriteSport favoriteCoachSport)
        {
            if (id != favoriteCoachSport.FavoriteSportsId)
            {
                return BadRequest();
            }
            _context.Entry(favoriteCoachSport).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }
        /*
         * allows for deletion of an object, finds it by id
         * if the id does not exist then it returns not found
         * otherwise it calls the database and removes the given object
         * and saves the changes to the database. 
         */
        [HttpDelete("{id}")]
        public ActionResult<CoachFavoriteSport> DeleteSport(int id)
        {
            var sportCoach = _context.coachFavoriteSportsTab.Find(id);

            if (sportCoach == null)
            {
                return NotFound();
            }
            _context.coachFavoriteSportsTab.Remove(sportCoach);
            _context.SaveChanges();

            return sportCoach;
        }
    }

}

