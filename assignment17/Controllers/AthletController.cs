﻿using assignment17.DTOs;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment17.Controllers
{
    /*
     * this class makes it possible to retrive, create, update and delete an athlet
     * in the database
     */
    [Route("api/[controller]")]
    [ApiController]
    
    public class AthletController : ControllerBase
    {
        private readonly CoachDBContext _context;

        private readonly IMapper _mapper;
        //establises a connection to the database 
        public AthletController(CoachDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        public ActionResult<IEnumerable<AthletDto>> GetAthletDto()
        {
            Athlet athlet1 = new Athlet();

            var list = new List<AthletDto>();
            var yes = _context.athletsTab.ToList();
            foreach(Athlet a in yes)
            {
                list.Add(_mapper.Map<AthletDto>(a));
            }

            AthletDto athletDto = _mapper.Map<AthletDto>(athlet1);

            return list;
        }
        //this method allows to get an athlet from the database
        /*[HttpGet]
        public ActionResult<IEnumerable<Athlet>> GetAthlet()
        {
          
            
            return _context.athletsTab;
        }*/
        //this method makes it possible to add an athlet to the database
        [HttpPost]
        public ActionResult<Athlet> ConsumeAthlet(Athlet athlet)
        {
            _context.athletsTab.Add(athlet);
            _context.SaveChanges();

            return athlet;
        }
        //this method finds an athlet by his Id and shows it in the terminal
        [HttpGet("{id}")]
        public ActionResult<AthletDto> GetAthletById(int id)
        {
            var athlet = _context.athletsTab.Find(id);
            //Athlet athlet1 = new Athlet();

            AthletDto athletDto = _mapper.Map<AthletDto>(athlet);
            
            return athletDto;
        }
        /*this method updates information of an athlet
         * it finds the athlet by his id and then updates the info on him
         */
        [HttpPut("{id}")]
        public ActionResult UpdateAthlet(int id, Athlet athlet)
        {
            if (id != athlet.Id)
            {
                return BadRequest();
            }
            _context.Entry(athlet).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }
        /*
         * this method deletes an athlet, it finds the athlet 
         * by his id and then deletes him
         */
        [HttpDelete("{id}")]
        public ActionResult<Athlet> DeleteAthlet(int id)
        {
            var athlet = _context.athletsTab.Find(id);
            if (athlet == null)
            {
                return NotFound();
            }
            _context.athletsTab.Remove(athlet);
            _context.SaveChanges();

            return athlet;  
        }
    }
}
