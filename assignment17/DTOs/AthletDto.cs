﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace assignment17.DTOs
{
    /*
     * this class is the Dto class for athlet, 
     * I have commented out the variables that I want to make invisable
     */
    public class AthletDto
    {
        public int Id { get; set; }

        //public int age { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        //public int? CoachId { get; set; }
        //public Coach Coach { get; set; }
    }
}
