﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment17.DTOs
{
    /*
     * the dto class for coach, I have commented out the variables
     * I want to hide to illustrate
     */
    public class CoachDto 
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Sport { get; set; }

        

        //public ICollection<Athlet> Athlets { get; set; }

        //public ICollection<CoachFavoriteSport> coachFavoriteSports { get; set; }
    }
}
