﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment17.DTOs
{
    //dto class of favsport, have commented out the variables that will be invisable
    public class FavSportDto
    {
        //public int Id { get; set; }
        public string sport { get; set; }

        public ICollection<CoachFavoriteSport> coachFavoriteSports { get; set; }
    }
}
