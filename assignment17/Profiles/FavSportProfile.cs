﻿using assignment17.DTOs;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment17.Profiles
{
    public class FavSportProfile : Profile
    {
        public FavSportProfile()
        {
            CreateMap<FavoriteSport, FavSportDto>();
        }
    }
}
