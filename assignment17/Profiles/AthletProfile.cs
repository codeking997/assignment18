﻿using assignment17.DTOs;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment17.Profiles
{
    public class AthletProfile : Profile
    {
        
        public AthletProfile()
        {
            CreateMap<Athlet, AthletDto>();
        }
    }
}
