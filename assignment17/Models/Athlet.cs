﻿using assignment17.DTOs;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment17

{
    public class Athlet
    {
        
        public int Id { get; set; }

        public int age { get; set; }


        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int? CoachId { get; set; }
        public Coach Coach { get; set; }


    }
}

