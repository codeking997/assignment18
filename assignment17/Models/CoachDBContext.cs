﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace assignment17
{
    public class CoachDBContext : DbContext
    {
        public CoachDBContext (DbContextOptions<CoachDBContext> options): base(options){
            
        }

        public DbSet<Coach> TrainerCoaches { get; set; }

        public DbSet<Athlet> athletsTab { get; set; }

        public DbSet<FavoriteSport> SportFavorite { get; set; }

        public DbSet<CoachFavoriteSport> coachFavoriteSportsTab { get; set; }

       /* protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=PC7585\\SQLEXPRESS;Initial Catalog=CoachDB; Integrated Security=True;");
        }*/
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Coach>().Property(e => e.AthletId).ValueGeneratedNever();
            //modelBuilder.Entity<Coach>().Property(e => e.AthletId).ValueGeneratedNever();
            modelBuilder.Entity<CoachFavoriteSport>()
               .HasKey(bc => new { bc.CoachId, bc.FavoriteSportsId });
            modelBuilder.Entity<CoachFavoriteSport>()
                .HasOne(bc => bc.Coach)
                .WithMany(b => b.coachFavoriteSports)
                .HasForeignKey(bc => bc.CoachId);
            modelBuilder.Entity<CoachFavoriteSport>()
                .HasOne(bc => bc.favoriteSport)
                .WithMany(c => c.coachFavoriteSports)
                .HasForeignKey(bc => bc.FavoriteSportsId);

        }

    }
}

