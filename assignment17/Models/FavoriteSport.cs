﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment17
{
    public class FavoriteSport
    {
        public int Id { get; set; }
        public string sport { get; set; }

        public ICollection<CoachFavoriteSport> coachFavoriteSports { get; set; }
    }
}
